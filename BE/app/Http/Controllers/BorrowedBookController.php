<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use App\Http\Requests\BorrowedBookRequest;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create_borrowed = BorrowedBook::create($request->only([
            'book_id', 'copies', 'patron_id'
        ]));
        
        $borrowedbook = BorrowedBook::with(['book'])->find($create_borrowed->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);

        return response()->json([
            'message' => 'Book borrowed successfully', 
            'borrowedbook' => $borrowedbook
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowedbook = BorrowedBook::with(['patron', 'book', 'book.category'])
            ->where('id', $id)->firstOrFail();
        return response()->json($borrowedbook);
    }
    
}
