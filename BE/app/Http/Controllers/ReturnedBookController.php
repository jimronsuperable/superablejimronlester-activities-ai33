<?php

namespace App\Http\Controllers;

use App\Models\ReturnedBook;
use App\Models\BorrowedBook;
use App\Http\Requests\ReturnedBookRequest;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(ReturnedBook::with([
            'book', 'patron', 'book.category'
        ])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrowedbook = BorrowedBook::where([
            ['book_id', $request->book_id],
            ['patron_id', $request->patron_id],
        ])->firstOrFail();
          
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returnedbook = ReturnedBook::with([
            'book', 'book.category', 'patron'
        ])->findOrfail($id);

        return response()->json($returnedbook);
    }

}
