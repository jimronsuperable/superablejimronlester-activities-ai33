<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Patron;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BorrowedBookTest extends TestCase
{
    /**
     * use RefreshDatabase; emp.db
     * A basic unit test example.
     *
     * @return void
     */
    public function test_stores_borrowed_books()
    {
        $category = Category::create([
            'category' => 'History'
        ]);

        $patron = Patron::factory()->create();
        $book = Book::factory(['category_id' => $category->id])->create();

        $borrowed = BorrowedBook::make([
            'copies' => 5, 
            'book_id' => $book->id, 
            'patron_id' => $patron->id
        ]);

        $this->call('POST', '/api/borrowedbook', $borrowed->toArray())->assertSuccessful();
        $this->assertDatabaseHas('borrowed_books', $borrowed->toArray());
    }

    public function test_gets_all_borrowed_books()
    {
        $this->call('GET', '/api/borrowedbook')->assertSuccessful();
    }

    public function test_gets_specific_borrowed_books()
    {
        $category = Category::create(['category' => 'Filipino']);
        $patron = Patron::factory()->create();
        $book = Book::factory(['category_id' => $category->id])->create();
        $borrowed = BorrowedBook::create([
            'copies' => 10, 
            'book_id' => $book->id, 
            'patron_id' => $patron->id
        ]);

        $this->call('GET', '/api/borrowedbook/'.$borrowed->id)
            ->assertJsonFragment($borrowed->toArray());
    }

}
