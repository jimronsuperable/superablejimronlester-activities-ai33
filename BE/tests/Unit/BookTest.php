<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_access_get_all_book(){
        $this->call('GET', 'api/books')->assertStatus(200);
    }

    public function test_create_book()
    {
        $category = Category::create(['category' => 'Computer']);
        $book = Book::factory()->make(['category_id' => $category->id]);

        $this->call('POST', '/api/books', $book->toArray());
        $this->assertDatabaseHas('books', $book->toArray());
    }

    public function test_update_book()
    {
        $category = Category::create(['category' => 'Computer II']);
        $book = Book::factory()->create(['category_id' => $category->id]);
        $data = [
            'name' => 'Laravel API', 
            'copies' => 40, 
            'category_id' => $category->id, 
            'author' => 'MemesArtisan'
        ];
        
        $expected_result = ['message' => 'Book has been updated!'];
        $this->call('PUT', '/api/books/'.$book->id, $data)->assertSee($expected_result);
    }

    public function test_delete_book()
    {
        $category = Category::create(['category' => 'Computer II']);
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->call('DELETE', '/api/books/'.$book->id);
        $this->assertDatabaseMissing('books', $book->toArray());
    }

    public function test_show_specific_book()
    {
        $category = Category::create(['category' => 'Computer II']);
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->call('GET', '/api/books/'.$book->id)->assertJson($book->toArray());
    }
}
