<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            ['category' => 'Computer'],
            ['category' => 'History'],
            ['category' => 'Clean Code'],
            ['category' => 'Filipino'],
        ];

        foreach ($category as $categ) {
            Category::create($categ);
        }
    }
}
