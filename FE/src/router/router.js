import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '@/components/pages/index/Dashboard'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/patron',
    name: 'Patron Management',
    component: () => import(/* webpackChunkName: "patronManagement" */ '@/components/pages/patron/Patron')
  },
  {
    path: '/books',
    name: 'Book Management',
    component: () => import(/* webpackChunkName: "bookManagement" */ '@/components/pages/book/Book')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "settings" */ '@/components/pages/settings/Settings')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router