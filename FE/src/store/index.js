import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);

import patrons from "./relative_modules/patron";
import books from "./relative_modules/books";
import borrowed from "./relative_modules/borrowed";
import returned from "./relative_modules/returned";

export default new Vuex.Store({
  modules: {
    patrons,
    books,
    borrowed,
    returned
  }
})